[TOC]

### 充值兑换流程

``` sequence
participant 用户
participant 地勤
participant 云服务
用户->用户:打开充值兑换二维码页面
地勤->用户:扫描用户二维码
地勤->云服务:获取商家充值可兑换商品
云服务-->>地勤:返回商品列表
地勤->地勤:选择用户兑换礼品
地勤->云服务:提交,记录充值兑换流水
云服务-->>地勤:兑换成功
云服务-->>用户:异步微信通知兑换明细
```